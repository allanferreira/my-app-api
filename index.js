var express = require('express')
var bodyParser = require('body-parser')
var fs = require('fs-extra')
var cors = require('cors')
var _ = require('underscore')
var chalk = require('chalk')
var colors = require('ansi-256-colors')


var jsonfile = require('jsonfile')
 
var autoresAPI = require(__dirname + '/public/autores.json')
var autoresAPIpath = __dirname + '/public/autores.json'

var app = express()
var router = express.Router()

app.use(cors());

app.use(express.static('public'))

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
    next()
})

app.use(bodyParser.urlencoded({extended : true}));

app.use(bodyParser.json({ type: 'application/json' }))
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))
app.use(bodyParser.text({ type: 'text/html' }))

app.get('/api/autores', function(req, res) {
    res.sendFile(__dirname + '/public/autores.json')
})

app.post('/api/autores', function(req, res) {
    
    autoresAPI.push(req.body)
    jsonfile.writeFile(autoresAPIpath, autoresAPI)
    res.status(200).send(JSON.stringify(autoresAPI))
    
})

app.get('*', function(req, res) {
    res.status(404).sendFile(__dirname + '/public/404.html')
})

app.listen(5000,function(){
    console.log('the ' + colors.fg.getRgb(5,0,1) + chalk.bold('API') + colors.reset + ' is running at:')
    console.log('')
    console.log('   ' + chalk.cyan('http://localhost:5000'))
    console.log('')
})